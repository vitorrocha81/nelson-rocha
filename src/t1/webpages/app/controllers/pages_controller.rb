class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  def index
    @pages = Page.all
  end
  
  def show
  end
   
  def new
    @page = Page.new
  end

  def create
    @page = Page.new(page_params)
    if @page.save
    redirect_to pages_path
    else
    render :new
    end
  end

  def edit
  end

  def update
    if @page.update(page_params)
    redirect_to pages_path
    else
    render :new
    end
  end

  def destroy
    if @page.destroy
    redirect_to pages_path
    end
  end

  private

    def set_page
    @page = Page.find(params[:id])
    end

    def page_params
    params.require(:page).permit(:title, :slug, :description, :body, :author)
    end
end